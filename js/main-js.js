// Створити масив, довжину та елементи якого задає користувач.
// Відсортувати масив за зростанням.
// Видалити елементи з масиву з 2 по 4 (включно!).

// У міру змін виводити вміст масиву на сторінку.

let arr = [];

for (let i = 0; i < 10; i++) {
  arr.push(prompt('Ведіть число ' + (i + 1) + '/10'));
}

console.log('Дані елементи: ' + arr);
console.log(arr);

function sortElems(a, b) {
	return a - b
}

console.log('Відстортовані елементи за зростанням: ' + arr.sort(sortElems));
console.log(arr);


const deleteElemArr = arr.splice(1, 3);

console.log('Видалені елементи: ' + deleteElemArr);
console.log(arr);
